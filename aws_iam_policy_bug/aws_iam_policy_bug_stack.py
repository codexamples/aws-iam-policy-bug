import aws_cdk as cdk
from aws_cdk import Stack, aws_eks, aws_iam
from constructs import Construct


class SubStack(cdk.NestedStack):
    def __init__(
        self,
        scope: Construct,
        id: str,
        role: aws_iam.Role,
    ) -> None:
        super().__init__(scope, id)

        iam_policy = aws_iam.Policy(
            self,
            "ProblematicPolicy",
            statements=[
                aws_iam.PolicyStatement(
                    effect=aws_iam.Effect.ALLOW,
                    actions=[
                        "secretsmanager:GetSecretValue",
                    ],
                    resources=[id],
                ),
            ],
        )

        role.attach_inline_policy(iam_policy)


class AwsIamPolicyBugStack(Stack):
    def __init__(self, scope: Construct, construct_id: str, **kwargs) -> None:
        super().__init__(scope, construct_id, **kwargs)

        role = aws_iam.Role(self, "role", assumed_by=aws_iam.AnyPrincipal())

        SubStack(self, "sub1", role)
        SubStack(self, "sub2", role)
