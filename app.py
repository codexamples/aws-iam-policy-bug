#!/usr/bin/env python3
import os

import aws_cdk as cdk

from aws_iam_policy_bug.aws_iam_policy_bug_stack import AwsIamPolicyBugStack

app = cdk.App()
AwsIamPolicyBugStack(app, "AwsIamPolicyBugStack")


app.synth()
