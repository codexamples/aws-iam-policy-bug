import aws_cdk as core
import aws_cdk.assertions as assertions

from aws_iam_policy_bug.aws_iam_policy_bug_stack import AwsIamPolicyBugStack

# example tests. To run these tests, uncomment this file along with the example
# resource in aws_iam_policy_bug/aws_iam_policy_bug_stack.py
def test_sqs_queue_created():
    app = core.App()
    stack = AwsIamPolicyBugStack(app, "aws-iam-policy-bug")
    template = assertions.Template.from_stack(stack)

#     template.has_resource_properties("AWS::SQS::Queue", {
#         "VisibilityTimeout": 300
#     })
